# Executables (local)
DOCKER = docker
DOCKER_COMPOSE = docker-compose
SYMFONY = symfony

# Misc
.DEFAULT_GOAL = help
.PHONY        = cs-fix help phpstan qa

##—— General ——————————————————————————————————————————————————

help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

##—— Tools ————————————————————————————————————————————————————

cs-fix: ## PHP Coding Standards Fixer
	@$(SYMFONY) php vendor/bin/php-cs-fixer fix

phpstan: ## Static Analysis Tool
	@$(SYMFONY) php vendor/bin/phpstan analyse

qa: cs-fix phpstan
