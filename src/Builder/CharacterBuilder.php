<?php

declare(strict_types=1);

namespace App\Builder;

use App\ArmorType\ArmorType;
use App\ArmorType\IceBlockType;
use App\ArmorType\LeatherArmorType;
use App\ArmorType\ShieldType;
use App\AttackType\AttackType;
use App\AttackType\BowType;
use App\AttackType\FireBoltType;
use App\AttackType\MultiAttackType;
use App\AttackType\TwoHandedSwordType;
use App\Character\Character;
use Psr\Log\LoggerInterface;

final class CharacterBuilder
{
    public const ATTACK_TYPE_BOW = 'bow';
    public const ATTACK_TYPE_FIRE_BOLT = 'fire_bolt';
    public const ATTACK_TYPE_SWORD = 'sword';
    public const ARMOR_TYPE_ICE_BLOCK = 'ice_block';
    public const ARMOR_TYPE_LEATHER_ARMOR = 'leather_armor';
    public const ARMOR_TYPE_SHIELD = 'shield';

    private string $armorType;
    /** @var string[] */
    private array $attackTypes;
    private int $baseDamage;
    private int $maxHealth;

    public function __construct(private readonly LoggerInterface $logger)
    {
    }

    public function buildCharacter(): Character
    {
        $this->logger->info('Creating a character', [
            'maxHealth' => $this->maxHealth,
            'baseDamage' => $this->baseDamage,
        ]);

        $attackTypes = array_map(fn (string $attackType) => $this->createAttackType($attackType), $this->attackTypes);
        if (1 === count($attackTypes)) {
            $attackType = $attackTypes[0];
        } else {
            $attackType = new MultiAttackType($attackTypes);
        }

        return new Character($this->maxHealth, $this->baseDamage, $attackType, $this->createArmorType());
    }

    private function createAttackType(string $attackType): AttackType
    {
        return match ($attackType) {
            self::ATTACK_TYPE_BOW => new BowType(),
            self::ATTACK_TYPE_FIRE_BOLT => new FireBoltType(),
            self::ATTACK_TYPE_SWORD => new TwoHandedSwordType(),
            default => throw new \RuntimeException('Invalid attack type given')
        };
    }

    private function createArmorType(): ArmorType
    {
        return match ($this->armorType) {
            self::ARMOR_TYPE_ICE_BLOCK => new IceBlockType(),
            self::ARMOR_TYPE_LEATHER_ARMOR => new LeatherArmorType(),
            self::ARMOR_TYPE_SHIELD => new ShieldType(),
            default => throw new \RuntimeException('Invalid armor type given')
        };
    }

    public function setArmorType(string $armorType): self
    {
        $this->armorType = $armorType;

        return $this;
    }

    public function setAttackType(string ...$attackTypes): self
    {
        $this->attackTypes = $attackTypes;

        return $this;
    }

    public function setBaseDamage(int $baseDamage): self
    {
        $this->baseDamage = $baseDamage;

        return $this;
    }

    public function setMaxHealth(int $maxHealth): self
    {
        $this->maxHealth = $maxHealth;

        return $this;
    }
}
