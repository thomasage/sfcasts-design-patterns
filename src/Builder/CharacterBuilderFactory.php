<?php

declare(strict_types=1);

namespace App\Builder;

use Psr\Log\LoggerInterface;

final class CharacterBuilderFactory
{
    public function __construct(private readonly LoggerInterface $logger)
    {
    }

    public function createBuilder(): CharacterBuilder
    {
        return new CharacterBuilder($this->logger);
    }
}
