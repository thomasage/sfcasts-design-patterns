<?php

declare(strict_types=1);

namespace App\Event;

use App\Character\Character;

final class FightStartingEvent
{
    public function __construct(public readonly Character $player, public readonly Character $ai)
    {
    }
}
