<?php

declare(strict_types=1);

namespace App\AttackType;

final class MultiAttackType implements AttackType
{
    /**
     * @param AttackType[] $attackTypes
     */
    public function __construct(private readonly array $attackTypes)
    {
    }

    public function performAttack(int $baseDamage): int
    {
        return $this->attackTypes[array_rand($this->attackTypes)]->performAttack($baseDamage);
    }
}
