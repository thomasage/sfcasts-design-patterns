<?php

declare(strict_types=1);

namespace App;

use App\Builder\CharacterBuilder;
use App\Builder\CharacterBuilderFactory;
use App\Character\Character;
use App\Event\FightStartingEvent;
use App\Observer\GameObserverInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

final class GameApplication
{
    /** @var GameObserverInterface[] */
    private array $observers = [];

    public function __construct(
        private readonly CharacterBuilderFactory $characterBuilderFactory,
        private readonly EventDispatcherInterface $eventDispatcher,
    ) {
    }

    public function play(Character $player, Character $ai): FightResult
    {
        $this->eventDispatcher->dispatch(new FightStartingEvent($player, $ai));

        $player->rest();

        $fightResult = new FightResult();
        while (true) {
            $fightResult->addRound();

            $damage = $player->attack();
            if (0 === $damage) {
                $fightResult->addExhaustedTurn();
            }

            $damageDealt = $ai->receiveAttack($damage);
            $fightResult->addDamageDealt($damageDealt);

            if ($this->didPlayerDie($ai)) {
                return $this->finishFightResult($fightResult, $player, $ai);
            }

            $damageReceived = $player->receiveAttack($ai->attack());
            $fightResult->addDamageReceived($damageReceived);

            if ($this->didPlayerDie($player)) {
                return $this->finishFightResult($fightResult, $ai, $player);
            }
        }
    }

    private function didPlayerDie(Character $player): bool
    {
        return $player->getCurrentHealth() <= 0;
    }

    private function finishFightResult(FightResult $fightResult, Character $winner, Character $loser): FightResult
    {
        $fightResult->setWinner($winner);
        $fightResult->setLoser($loser);

        $this->notify($fightResult);

        return $fightResult;
    }

    private function notify(FightResult $fightResult): void
    {
        foreach ($this->observers as $observer) {
            $observer->onFightFinished($fightResult);
        }
    }

    public function createCharacter(string $character): Character
    {
        return match (strtolower($character)) {
            'fighter' => $this->createCharacterBuilder()
                ->setMaxHealth(90)
                ->setBaseDamage(12)
                ->setAttackType(CharacterBuilder::ATTACK_TYPE_SWORD)
                ->setArmorType(CharacterBuilder::ARMOR_TYPE_SHIELD)
                ->buildCharacter(),
            'archer' => $this->createCharacterBuilder()
                ->setMaxHealth(80)
                ->setBaseDamage(10)
                ->setAttackType(CharacterBuilder::ATTACK_TYPE_BOW)
                ->setArmorType(CharacterBuilder::ARMOR_TYPE_LEATHER_ARMOR)
                ->buildCharacter(),
            'mage' => $this->createCharacterBuilder()
                ->setMaxHealth(70)
                ->setBaseDamage(8)
                ->setAttackType(CharacterBuilder::ATTACK_TYPE_FIRE_BOLT)
                ->setArmorType(CharacterBuilder::ARMOR_TYPE_ICE_BLOCK)
                ->buildCharacter(),
            'mage_archer' => $this->createCharacterBuilder()
                ->setMaxHealth(90)
                ->setBaseDamage(12)
                ->setAttackType(CharacterBuilder::ATTACK_TYPE_FIRE_BOLT, CharacterBuilder::ATTACK_TYPE_BOW)
                ->setArmorType(CharacterBuilder::ARMOR_TYPE_SHIELD)
                ->buildCharacter(),
            default => throw new \RuntimeException('Undefined Character'),
        };
    }

    private function createCharacterBuilder(): CharacterBuilder
    {
        return $this->characterBuilderFactory->createBuilder();
    }

    /**
     * @return string[]
     */
    public function getCharactersList(): array
    {
        return [
            'fighter',
            'mage',
            'archer',
            'mage_archer',
        ];
    }

    public function subscribe(GameObserverInterface $observer): void
    {
        if (!in_array($observer, $this->observers, true)) {
            $this->observers[] = $observer;
        }
    }

    public function unsubscribe(GameObserverInterface $observer): void
    {
        $key = array_search($observer, $this->observers, true);
        if (false !== $key) {
            unset($this->observers[$key]);
        }
    }
}
