<?php

declare(strict_types=1);

namespace App\ArmorType;

use App\Dice;

final class ShieldType implements ArmorType
{
    public function getArmorReduction(int $damage): int
    {
        $changeToBlock = Dice::roll(100);

        return $changeToBlock > 80 ? $damage : 0;
    }
}
