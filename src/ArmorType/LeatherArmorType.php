<?php

declare(strict_types=1);

namespace App\ArmorType;

final class LeatherArmorType implements ArmorType
{
    public function getArmorReduction(int $damage): int
    {
        return (int) floor($damage * 0.25);
    }
}
