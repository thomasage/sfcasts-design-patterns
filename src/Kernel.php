<?php

declare(strict_types=1);

namespace App;

use App\Observer\GameObserverInterface;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;

final class Kernel extends BaseKernel implements CompilerPassInterface
{
    use MicroKernelTrait;

    private const TAG_GAME_OBSERVER = 'game.observer';

    public function process(ContainerBuilder $container): void
    {
        $definition = $container->findDefinition(GameApplication::class);
        $taggedObservers = $container->findTaggedServiceIds(self::TAG_GAME_OBSERVER);
        foreach ($taggedObservers as $id => $tags) {
            $definition->addMethodCall('subscribe', [new Reference($id)]);
        }
    }

    protected function build(ContainerBuilder $container): void
    {
        $container
            ->registerForAutoconfiguration(GameObserverInterface::class)
            ->addTag(self::TAG_GAME_OBSERVER);
    }
}
