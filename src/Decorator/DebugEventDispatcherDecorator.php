<?php
declare(strict_types=1);

namespace App\Decorator;

use Symfony\Component\DependencyInjection\Attribute\AsDecorator;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

#[AsDecorator('event_dispatcher')]
final class DebugEventDispatcherDecorator implements EventDispatcherInterface
{
    public function __construct(private readonly EventDispatcherInterface $innerEventDispatcher)
    {
    }

    public function addListener(string $eventName, $listener, int $priority = 0): void
    {
        $this->innerEventDispatcher->addListener($eventName, $listener, $priority);
    }

    public function addSubscriber(EventSubscriberInterface $subscriber): void
    {
        $this->innerEventDispatcher->addSubscriber($subscriber);
    }

    public function removeListener(string $eventName, callable $listener): void
    {
        $this->innerEventDispatcher->removeListener($eventName, $listener);
    }

    public function removeSubscriber(EventSubscriberInterface $subscriber): void
    {
        $this->innerEventDispatcher->removeSubscriber($subscriber);
    }

    public function getListeners(string $eventName = null): array
    {
        return $this->innerEventDispatcher->getListeners($eventName);
    }

    public function dispatch(object $event, string $eventName = null): object
    {
        dump('--------------------');
        dump('Dispatching event:'.$event::class);
        dump('--------------------');

        return $this->innerEventDispatcher->dispatch($event, $eventName);
    }

    public function getListenerPriority(string $eventName, callable $listener): ?int
    {
        return $this->innerEventDispatcher->getListenerPriority($eventName, $listener);
    }

    public function hasListeners(string $eventName = null): bool
    {
        return $this->innerEventDispatcher->hasListeners($eventName);
    }
}
